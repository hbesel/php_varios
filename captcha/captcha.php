<?php 
session_start();
$im = @imagecreate(180, 80);


$rnd=randomizar();
$color_fondo = imagecolorallocate ($im, $rnd['FondoR'], $rnd['FondoG'], $rnd['FondoB']);
$color_texto = imagecolorallocate ($im, 0, 128, 6);

// cargofuente de archivo
$font = 'underwood_champion.ttf';

// Sombra colorida para marear con diferentes valores a la del captcha real
$captchaColor = imagecolorallocate($im, $rnd['SombR'], $rnd['SombG'], $rnd['SombB']);
$charCaptcha=0;
$sumaX=0;
while ($charCaptcha < 6) {
	$rnd=randomizar();
	imagettftext($im, $rnd['SombT'], $rnd['SombR'], $rnd['SombX']+$sumaX, $rnd['SombY'], $captchaColor, $font, $rnd['char']);
	$charCaptcha++;
	$sumaX=$sumaX+30;
}

// texto real del captcha
//color del texto captcha siempre es de negro a gris oscuro, para dar contraste
$captchaGris = imagecolorallocate($im, $rnd['CaptC'], $rnd['CaptC'], $rnd['CaptC']);
$captchaFinal="";
$charCaptcha=0;
$sumaX=0;
while ($charCaptcha < 6) {
	$rnd=randomizar();
	imagettftext($im, $rnd['CaptT'], $rnd['CaptR'], $rnd['CaptX']+$sumaX, $rnd['CaptY'], $captchaGris, $font, $rnd['char']);
	$captchaFinal=$captchaFinal.$rnd['char'];
	$charCaptcha++;
	$sumaX=$sumaX+22;
}

//
header ("Content-type: image/png");
imagepng($im);
$_SESSION['codigos']=$captchaFinal;


// ----------------------------------------------------------------------
function randomizar(){

//quito 1, l, 0, O y o para no confundir al usuario.
$numero=rand(2,9);
$mayus=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","P","Q","R","S","T","U","V","W","X","Y","Z");
$minus=array("a","b","c","d","e","f","g","h","i","j","k","m","n","p","q","r","s","t","u","v","w","x","y","z");

// elijo un tipo de elemento al azar: may, min o num para siguiente simobolo del captcha.
$rndTipo = rand(1,3);
switch ($rndTipo) {
    case 1:
        $char=$minus[rand(0,23)];
        break;
    case 2:
        $char=$mayus[rand(0,24)];
        break;
    case 3:
        $char=$numero;
        break;
}

// genero valores al azar para X/Y, tamaño, rotacion y colores del captcha y su sombra y colores de fondo.
// ademas asigno el siguiente simbolo del captcha,
$rnd=[
	"FondoR"=>rand(150,255),
	"FondoG"=>rand(150,255), 
	"FondoB"=>rand(150,255), 
	"CaptX"=>rand(10,20), 
	"CaptY"=>rand(42,55), 
	"CaptR"=>rand(-6,6), 
	"CaptT"=>rand(25,28),
	"CaptC"=>rand(0,100),
	"SombX"=>rand(10,20),
	"SombY"=>rand(42,55), 
	"SombR"=>rand(-3,4),
	"SombT"=>rand(25,47), 
	"SombR"=>rand(120,200), 
	"SombG"=>rand(120,200), 
	"SombB"=>rand(120,200),
	"char"=>$char,
	];
return $rnd;
}
?>